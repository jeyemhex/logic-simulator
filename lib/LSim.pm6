use Device;

grammar LSim {
    rule TOP { <device>+ }

    rule device          { <bus-def> <bus-def-list> '{' <stmt-list> '}' }

    rule stmt-list      { <stmt>+ }

    proto rule stmt     { * }

    rule stmt:<define>  { "define" <bus-def-list> ';' }

    rule stmt:<assign>  { <bus-list> "=" [<name> <bus-list> | <value>] ';' }

    rule stmt:<output>  { "output" <bus-list> ';' }

    rule stmt:<clock>  { "clock" <bus> <value> ';' }

    rule bus-def-list   { '(' <bus-def>* % ',' ')' | <bus-def>+ % ',' }

    token bus-def       { <name> [ '[' <number> ']' ]? }

    rule bus-list       { '(' <bus>* % ',' ')' | <bus>+ % ',' }

    regex bus           { <name> [ '[' [<number> | <range>] ']' ]? | <value> } 

    token name          { [<alnum> | "-"]+ }

    token value         { '#' <[ 0..9 a..f ]>+ }

    token range         { <number> ":" <number> }

    token number        { <digit>+ }
}

class LSim-actions {
    my %device-bank;

    method TOP($/) { 
        my %devices = $<device>>>.made;
        make %device-bank<main> or die "No device 'main' found, aborting!";
    }

    method device($/) {
        my $device      = $<bus-def>.made;
        my @inputs      = $<bus-def-list>.made;
        my @statements  = $<stmt-list>.made;
        my (@devices, @state, @outputs, @instructions);
        my %state-lookup;


        my $n_inputs = 0;
        for @inputs -> $input {
            for 1..$input<size> -> $i {
                %state-lookup{$input<name> ~ ":" ~ $i} = @state.elems;
                push @state, (rand < .5);
                $n_inputs++;
            }
        }

        for @statements -> $stmt {
            given $stmt<type> {

                when "define" {
                    for $stmt<bus-def-list>.List -> $bus {
                        for 1 .. $bus<size> -> $i {
                            %state-lookup{$bus<name> ~ ":" ~ $i} = @state.elems;
                            push @state, (rand < .5);
                        }
                    }

                }

                when "assign-from-device" {
                    my $instruction = {inputs  => [],
                                       outputs => [],
                                       device  => -1 };

                    for $stmt<input-list>.List -> $input {
                        for $input<first> ... $input<last> -> $i {
                            push $instruction<inputs>, %state-lookup{$input<name> ~ ":" ~ $i};
                        }
                    }

                    for $stmt<output-list>.List -> $output {
                        for $output<first> .. $output<last> -> $i {
                            push $instruction<outputs>, %state-lookup{$output<name> ~ ":" ~ $i};
                        }
                    }

                    if $stmt<device> !~~ "nand" {
                        $instruction<device> = @devices.elems;
                        try {
                            push @devices, %device-bank{$stmt<device>}.instance();
                        }

                        die "No such device $stmt<device>" if $!;
                    }
                    push @instructions, $instruction;
                }

                when "assign-from-value" {
                    my $instruction = { inputs  => [],
                                        outputs => [],
                                        device  => -2 };


                    for $stmt<output-list>.List -> $output {
                        for $output<first> .. $output<last> -> $i {
                            push $instruction<outputs>, %state-lookup{$output<name> ~ ":" ~ $i};
                        }
                    }
                    push $instruction<inputs>, $stmt<input-value>;
                    push @instructions, $instruction;
                }

                when "clock" {
                    my $instruction = { inputs  => [],
                                        outputs => [],
                                        time    => $stmt<time>,
                                        device  => -3 };
                    my $output = $stmt<output>;
                    push $instruction<outputs>, %state-lookup{$output<name> ~ ":" ~ $output<first>};
                    push @instructions, $instruction;
                }

                when "output" {
                    for $stmt<bus-list>.List -> $output {
                        for $output<first> .. $output<last> -> $i {
                            push @outputs, %state-lookup{$output<name> ~ ":" ~ $i};
                        }
                    }
                }
            }
        }

        %device-bank{$device<name>} = Device.new(name         => $device<name>,
                                                 devices      => @devices,
                                                 state        => @state,
                                                 instructions => @instructions,
                                                 outputs      => @outputs,
                                                 n_inputs     => $n_inputs,
                                                 n_outputs    => @outputs.elems,
                                                 state-lookup => %state-lookup );
        make %device-bank;
    }

    method stmt-list($/) {
        make ( $<stmt>>>.made );
    }

    proto method stmt($/) { * }

    method stmt:<define>($/) {
        make { type     => "define",
               bus-def-list => $<bus-def-list>.made };

    }

    method stmt:<assign>($/) {
        if $<name> {
            make { type         => "assign-from-device",
                   input-list   => $<bus-list>[1].made,
                   output-list  => $<bus-list>[0].made,
                   device       => $<name>.made };
        } else {
            make { type         => "assign-from-value",
                   output-list  => $<bus-list>[0].made,
                   input-value  => $<value>.made };
        }
    }

    method stmt:<output>($/) {
        make { type     => "output",
               bus-list => $<bus-list>.made };
    }

    method stmt:<clock>($/) {
        make { type    => "clock",
               output  => $<bus>.made,
               time    => $<value>.made };
    }

    method bus-def-list($/) {
        make $<bus-def>>>.made;
    }

    method bus-def($/) {
        my $size = 1;
        $size = $<number>.Int if $<number>;
        make {  name  => $<name>.made,
                size  => $size };
    }

    method bus-list($/) {
        make $<bus>>>.made;
    }

    method bus($/) {

        my $type = "bus";

        my ($first, $last);
        if $<range> {
            ($first, $last) = $<range>.made;
        } else {
            $first = 1;
            $first = $<number>.Int if $<number>;
            $last = $first;
        }

        make {  type  => $type,
                name  => $<name>.made,
                first => $first,
                last  => $last };
    }

    method name($/) { make $/.Str }

    method range($/) {
        make ($<number>[0].Int, $<number>[1].Int);
    }

    method value($/) { 
        my $str_char = "0" ~ "x" ~ substr($/.Str, 1, *);
        make $str_char.Int;
    }

    method size($/) { make $/.Int }
}


