my $device-id = 0;

class Device {
    has $.name;
    has $.id is rw;
    has @.devices;
    has @.state;
    has @.prev-state;
    has @.outputs;
    has $.n_inputs;
    has $.n_outputs;
    has @.instructions;
    has %.state-lookup;

    method instance() {
        my $new-instance = $.perl.EVAL;
        $new-instance.id = $device-id++;
        return $new-instance;
    }

    method step(@inputs, $time) {
        die "Incorrect number of inputs to $!name" if @inputs.elems != $!n_inputs;

        for ^@inputs.elems -> $i {
            @!state[$i] = @inputs[$i];
        }
        @!prev-state = @!state.clone;
        $_ = 0 for @!state;

        for @!instructions -> $instruction {
            my (@device-in, @device-out);

            for $instruction<inputs>.List -> $i {
                push @device-in, @!prev-state[$i];
            }

            if $instruction<device> == -1 {
                if all(@device-in) {
                    push @device-out, False;
                } else {
                    push @device-out, True;
                }

            } elsif $instruction<device> >= 0 {
                my $device = @!devices[$instruction<device>];
                @device-out = $device.step(@device-in, $time);

            } elsif $instruction<device> == -2 {
                my $tmp = $instruction<inputs>[0];
                for $instruction<outputs>.elems-1, $instruction<outputs>.elems-2 ... 0 -> $i {
                    if $tmp >= 2**$i {
                        push @device-out, True;
                        $tmp -= 2**$i;
                    } else {
                        push @device-out, False;
                    }
                }
            } elsif $instruction<device> == -3 {
                if ($time / $instruction<time>).Int %% 2 {
                    push @device-out, False;
                } else {
                    push @device-out, True;
                }
            }

            for ^$instruction<outputs>.elems -> $i {
                @!state[ $instruction<outputs>[$i] ] = @device-out[$i];
            }
        }

        my @outputs;
        for @!outputs -> $i {
            push @outputs, @!state[$i];
        }

        die "Incorrect number of outputs to $!name" if @outputs.elems != $!n_outputs;
        return @outputs;
    }

    method bus($bus-name) {
        my @out;
        for %!state-lookup.keys -> $k {
            if $k ~~ /$bus-name ":" (<[0..9]>+) / {
                @out[ $/[0].Int - 1 ] = @!state[ %!state-lookup{$k} ] ?? "1" !! "0";
            }
        }
        return @out;
    }
}

