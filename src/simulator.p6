#!/usr/bin/env perl6
#EJH# use Grammar::Tracer;
use LSim;
our $device-id = 0;
our $time = 0;


sub MAIN(*@fnames) {
    say @fnames.join(", ");

    my $source;
    for @fnames -> $fname {
        $source ~= $fname.IO.slurp or die "Unable to read file";
    }
    my $actions = LSim-actions.new;

    print "Parsing circuit...";
    my $circuit = LSim.parse($source, actions => $actions).made || die "Unable to parse source:\n\n$source";
    say " Done!";

    print "\n";
    say "t = ", $time*1 , " steps";
    say "a = " ~ $circuit.bus("a");
    say "b = " ~ $circuit.bus("b");
    say "c = " ~ $circuit.bus("c");
    say "\n";
    loop {
        $time += 1;
        $circuit.step( () , $time);
        print "\e[6A\r";
        say "t = ", $time*1 , " steps";
        say "a = " ~ $circuit.bus("a");
        say "b = " ~ $circuit.bus("b");
        say "c = " ~ $circuit.bus("c");
        say "\n";
        sleep 0.5;
    }
}
